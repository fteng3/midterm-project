//
// Created by pteng on 3/24/2020.
//
#include "ppm_io.h"
#include <stdio.h>
#include "readInput.h"
#include <stdlib.h>
#include <math.h>


/*
 * Exposure method
 */
Image * exposure(Image * input, double EV) {
    double factor = pow(2, EV);
    Image * new_img = malloc(sizeof(Image));
    new_img->data = malloc(sizeof(Pixel) * input->cols * input->rows);
    new_img->cols = input->cols;
    new_img->rows = input->rows;
    for (int i = 0; i < input->rows; i++) {
        for (int j = 0; j < input->cols; j++) {
       	    int red = factor * input->data[i*input->cols + j].r;
	        if (red > 255) {
	            new_img->data[i * input->cols + j].r = 255;
	        } else {
	            new_img->data[i * input->cols + j].r = red;
	        } 
            int green = factor * input->data[i*input->cols + j].g;
            if (green > 255) {
                new_img->data[i * input->cols + j].g = 255;
            } else {
                new_img->data[i * input->cols + j].g = green;
            }

            int blue = factor * input->data[i*input->cols + j].b;
            if (blue > 255) {
                new_img->data[i * input->cols + j].b = 255;
            } else {
                new_img->data[i * input->cols + j].b = blue;
            }
        }
    }
    return new_img;
}

/*
 * Blend method
 */
Image * blend(Image * input1, Image * input2, double alpha) {
    Image *output = malloc(sizeof(Image));
    int outcols, outrows;
    if (input1->cols > input2->cols) {
        outcols = input1->cols;
    } else {
        outcols = input2->cols;
    }
    if (input1->rows > input2->rows) {
        outrows = input1->rows;
    } else {
        outrows = input2->rows;
    }
    output->cols = outcols;
    output->rows = outrows;
    output->data = malloc(sizeof(Pixel) * outcols * outrows);
    Pixel *in1 = input1->data;
    Pixel *in2 = input2->data;

    if ((input1->cols != input2->cols) || (input1->rows != input2->rows)) {
        if (input1->cols > input2->cols) {
            // Case 1: 1->cols > 2->cols, 1->rows > 2->rows
            if (input1->rows > input2->rows) {
                for (int i = 0; i < outrows; i++) {
                    if (i > input2->rows) {
                        for (int j = 0; j < outcols; j++) {
                            output->data[i * outcols + j].r = in1[i * outcols + j].r;
                            output->data[i * outcols + j].g = in1[i * outcols + j].g;
                            output->data[i * outcols + j].b = in1[i * outcols + j].b;
                        }
                    } else {
                        for (int j = 0; j < outcols; j++) {
                            if (j > input2->cols) {
                                output->data[i * outcols + j].r = in1[i * outcols + j].r;
                                output->data[i * outcols + j].g = in1[i * outcols + j].g;
                                output->data[i * outcols + j].b = in1[i * outcols + j].b;
                            } else {
                                output->data[i * outcols + j].r =
                                        alpha * in1[i * outcols + j].r + (1 - alpha) * in2[i * outcols + j].r;
                                output->data[i * outcols + j].g =
                                        alpha * in1[i * outcols + j].g + (1 - alpha) * in2[i * outcols + j].g;
                                output->data[i * outcols + j].b =
                                        alpha * in1[i * outcols + j].b + (1 - alpha) * in2[i * outcols + j].b;
                            }
                        }
                    }
                }
            } else {
                // Case 2: 1->cols > 2->cols, 2->rows > 1->rows
                for (int i = 0; i < input1->rows; i++) {
                    for (int j = 0; j < input1->cols; j++) {
                        if (j > input2->cols) {
                            output->data[i * outcols + j].r = in1[i * outcols + j].r;
                            output->data[i * outcols + j].g = in1[i * outcols + j].g;
                            output->data[i * outcols + j].b = in1[i * outcols + j].b;
                        } else {
                            output->data[i * outcols + j].r =
                                    alpha * in1[i * outcols + j].r + (1 - alpha) * in2[i * outcols + j].r;
                            output->data[i * outcols + j].g =
                                    alpha * in1[i * outcols + j].g + (1 - alpha) * in2[i * outcols + j].g;
                            output->data[i * outcols + j].b =
                                    alpha * in1[i * outcols + j].b + (1 - alpha) * in2[i * outcols + j].b;
                        }
                    }
                }
                // Fill in leftover input2 rows
                for (int i = input1->rows; i < input2->rows; i++) {
                    for (int j = 0; j < input1->cols; j++) {
                        if (j > input2->cols) {
                            output->data[i * outcols + j].r = 0;
                            output->data[i * outcols + j].g = 0;
                            output->data[i * outcols + j].b = 0;
                        } else {
                            output->data[i * outcols + j].r = in2[i * outcols + j].r;
                            output->data[i * outcols + j].g = in2[i * outcols + j].g;
                            output->data[i * outcols + j].b = in2[i * outcols + j].b;
                        }
                    }
                }
            }
        } else {
            // Case 3: 2->cols > 1->cols, 1->rows > 2->rows
            if (input1->rows > input2->rows) {
                for (int i = 0; i < input2->rows; i++) {
                    for (int j = 0; j < input2->cols; j++) {
                        if (j <= input1->cols) {
                            output->data[i * outcols + j].r =
                                    alpha * in1[i * outcols + j].r + (1 - alpha) * in2[i * outcols + j].r;
                            output->data[i * outcols + j].g =
                                    alpha * in1[i * outcols + j].g + (1 - alpha) * in2[i * outcols + j].g;
                            output->data[i * outcols + j].b =
                                    alpha * in1[i * outcols + j].b + (1 - alpha) * in2[i * outcols + j].b;
                        } else {
                            output->data[i * outcols + j].r = in2[i * outcols + j].r;
                            output->data[i * outcols + j].g = in2[i * outcols + j].g;
                            output->data[i * outcols + j].b = in2[i * outcols + j].b;
                        }
                    }
                }
                // Fill in leftover input1 rows
                for (int i = input2->rows; i < input1->rows; i++) {
                    for (int j = 0; j < input2->cols; j++) {
                        if (j <= input1->cols) {
                            output->data[i * outcols + j].r = in1[i * input2->cols + j].r;
                            output->data[i * outcols + j].g = in1[i * input2->cols + j].g;
                            output->data[i * outcols + j].b = in1[i * input2->cols + j].b;
                        } else {
                            output->data[i * outcols + j].r = 0;
                            output->data[i * outcols + j].g = 0;
                            output->data[i * outcols + j].b = 0;
                        }
                    }
                }
            } else {
                // Case 4: 2->cols > 1->cols, 2->rows > 1-> cols
                for (int i = 0; i < outrows; i++) {
                    for (int j = 0; j < outcols; j++) {
                        if (j > input2->cols) {
                            output->data[i * outcols + j].r = in1[i * outcols + j].r;
                            output->data[i * outcols + j].g = in1[i * outcols + j].g;
                            output->data[i * outcols + j].b = in1[i * outcols + j].b;
                        } else {
                            output->data[i * outcols + j].r =
                                    alpha * in1[i * outcols + j].r + (1 - alpha) * in2[i * outcols + j].r;
                            output->data[i * outcols + j].g =
                                    alpha * in1[i * outcols + j].g + (1 - alpha) * in2[i * outcols + j].g;
                            output->data[i * outcols + j].b =
                                    alpha * in1[i * outcols + j].b + (1 - alpha) * in2[i * outcols + j].b;
                        }
                    }
                }
            }
        }
    } else {
    // Case 5: rows and cols match for both inputs
        printf(" ROWS + COLS MATCH \n");
        for (int i = 0; i < outrows; i++) {
            for (int j = 0; j < outcols; j++) {
                output->data[i*outcols+j].r = alpha*in1[i*outcols+j].r + (1-alpha)*in2[i*outcols+j].r;
                output->data[i*outcols+j].g = alpha*in1[i*outcols+j].g + (1-alpha)*in2[i*outcols+j].g;
                output->data[i*outcols+j].b = alpha*in1[i*outcols+j].b + (1-alpha)*in2[i*outcols+j].b;
            }
        }
    }
    return output;
}


/*
 * zoom_in method
 */
Image * zoom_in(Image * input) {
    Image * new_img = malloc(4 * sizeof(input));
    new_img->data = malloc(4 * sizeof(Pixel) * input->cols * input->rows);
    int i = 0;
    for (int j = 0; j < input->cols*input->rows; j++) {
        new_img->data->r = input->data->r;
        new_img->data->g = input->data->g;
        new_img->data->b = input->data->b;
        new_img->data += input->cols; //moving down one

        new_img->data->r = input->data->r;
        new_img->data->g = input->data->g;
        new_img->data->b = input->data->b;
        new_img->data++; // moving to the right one

        new_img->data->r = input->data->r;
        new_img->data->g = input->data->g;
        new_img->data->b = input->data->b;
        new_img->data -= input->cols; //moving up one

        new_img->data->r = input->data->r;
        new_img->data->g = input->data->g;
        new_img->data->b = input->data->b;

        input->data++;
        new_img->data++;
        i++;
        if (i == input->cols) {
            new_img->data += input->cols;
            i = 0;
        }
    }
    return new_img;
}


/*
 * zoom_out method
 */
Image * zoom_out(Image * input) {
  int size = 0.25 * sizeof(input);
    Image * output = malloc (size);
    if (input->rows % 2 == 1) { input->rows--; }
    if (input->cols % 2 == 1) { input->cols--; }

    for (int row = 0; row < input->rows; row = row + 2) {
        for (int col = 0; col < input->cols; col = col + 2) {
            int red = 0, green = 0, blue = 0;
            red = input->data[row*input->cols + col].r +
                    input->data[row*input->cols + col + 1].r +
                    input->data[(row+1)*input->cols + col].r +
                    input->data[(row+1)*input->cols + col + 1].r;
            green = input->data[row*input->cols + col].g +
                  input->data[row*input->cols + col + 1].g +
                  input->data[(row+1)*input->cols + col].g +
                  input->data[(row+1)*input->cols + col + 1].g;
            blue = input->data[row*input->cols + col].b +
                  input->data[row*input->cols + col + 1].b +
                  input->data[(row+1)*input->cols + col].b +
                  input->data[(row+1)*input->cols + col + 1].b;
            output->data->r = red / 4;
            output->data->g = green / 4;
            output->data->b = blue / 4;
        }
    }
    return output;
}

/*
 * pointilism method
 */
Image * pointilism(Image * input) {
    Image * output = malloc(sizeof(input));
    int size = input->cols * input->rows;
    int randpix = size * 0.03;
    for (int i = 0; i < randpix; i++) {
        int a = rand() % input->cols;
        int b = rand() % input->rows;
        int radius = rand() % 5 + 1;
        for (int y = 0; y < input->rows; y++) {
            for (int x = 0; x < input->cols; x++) {
                if (pow((x-a), 2) + pow((y-b), 2) <= pow(radius, 2)) {
                    output->data[y*input->cols + x].r = input->data[a*input->cols + b].r;
                    output->data[y*input->cols + x].g = input->data[a*input->cols + b].g;
                    output->data[y*input->cols + x].b = input->data[a*input->cols + b].b;
                }
            }
        }
    }
    return input;
}

/*
 * swirl method
 */
Image * swirl(Image * input, int cx, int cy, int scale) {
    Image * output = malloc(sizeof(input));
    // Start with black screen
    for (int a = 0; a < input->rows; a++) {
        for (int b = 0; b < input->cols; b++) {
            output->data[a*input->cols + b].r = 0;
            output->data[a*input->cols + b].g = 0;
            output->data[a*input->cols + b].b = 0;
        }
    }

    // add in transformed points
    for (int i = 0; i < input->rows; i++) {
        for (int j = 0; j < input->cols; j++) {
            double alpha = (sqrt(pow(i-cx, 2) + pow(j-cy, 2))) / scale;
            int col = (i-cx)*cos(alpha) - (j-cx)*sin(alpha) + cx;
            int row = (i-cx)*sin(alpha) + (j-cy)*cos(alpha) + cy;
            output->data[row*input->cols + col].r = input->data[i*input->cols + j].r;
            output->data[row*input->cols + col].g = input->data[i*input->cols + j].g;
            output->data[row*input->cols + col].b = input->data[i*input->cols + j].b;
        }
    }
    return output;
}

// #define PI 3.14159
/*
 * blur method
 */
Image * blur(Image * input, double sigma) {
    input->data[5].r = (int) sigma;
    return input;
}
