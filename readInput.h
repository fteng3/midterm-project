//
// Created by pteng on 3/28/2020.
//
#include "ppm_io.h"
#include "imageManip.h"

#ifndef UNTITLED_READINPUT_H
#define UNTITLED_READINPUT_H

int readInput(int argc, char *argv[]);

int checkname(char *func);

int checkargs(int f, int argc, char *argv[]);

Image * run(Image* img, int func, char *argv[]);

#endif //UNTITLED_READINPUT_H
