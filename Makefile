CC=gcc
CONSERVATIVE_FLAGS=-std=c99 -pedantic -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

project: project.o ppm_io.o imageManip.o readInput.o
	$(CC) -o project project.o ppm_io.o imageManip.o readInput.o -lm

project.o: project.c ppm_io.h imageManip.h readInput.h
	$(CC) $(CFLAGS) -c project.c

ppm_io.o: ppm_io.c ppm_io.h
	$(CC) $(CFLAGS) -c ppm_io.c

imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

readInput.o: readInput.c readInput.h
	$(CC) $(CFLAGS) -c readInput.c

clean:
	rm -f *.o ppm
