#include <stdio.h>
#include "ppm_io.h"
#include "imageManip.h"
#include <stdlib.h>
#include <assert.h>
int main(int argc, char* argv[]) {
  FILE *fp;
  fp = fopen(argv[1], "rb");
  assert(fp);
  Image * input = malloc(sizeof(Image));
  input = read_ppm(fp);
  assert(input != NULL);
  assert(argv[2]);
  FILE *outp;
  outp = fopen(argv[2], "wb");
  write_ppm(outp, input);
  free(input->data);
  free(input);
  fclose(fp);
  fclose(outp);
  return 1;
}
