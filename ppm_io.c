// Fei Teng
// fteng3
// Kevin Velasquez
// kvelasq1
// __Add your name and JHED above__
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdio.h>
#include <stdlib.h>

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {
    // check that fp is not NULL
    assert(fp);

    // Scans format, checks if first line is P6\n
    char format[3];
    fgets(format, 3, fp);
    if (format[0] != 'P' || format[1] != '6') {
        printf("Not P6 format!\n");
        return NULL;
    }
    
    // Ignore lines starting with #
    unsigned char c;
    c = fgetc(fp);
    if (c == '#') {
        while (c != '\n') {
            fscanf(fp, "%c", &c);
        }
        c = ungetc(c, fp);
    }
    c = ungetc(c, fp);

    // Scan cols and rows, reject if there are not 2
    int cols, rows, color;
    int scanned = fscanf(fp, " %d %d %d ", &cols, &rows, &color);
    if  (scanned != 3) {
        printf("%d \n", scanned);
        printf("Did not get 3 ints for cols, rows, and color\n");
        return NULL;
    }
    if (color != 255) {
      printf("Color not 255");
      return NULL;
    }
    assert(color == 255);

    // Ignore whitespaces
    fscanf(fp, " %c", &c);
    ungetc(c, fp);

    // Scans pixel array from fp
    Pixel *data = malloc(sizeof(Pixel)*rows*cols);
    int pixels_read = fread(data, sizeof(Pixel), cols*rows, fp);
    assert(pixels_read == cols * rows);
    Image* img = malloc(sizeof(Image));
    img->data = data;
    img->rows = rows;
    img->cols = cols;
    
    return img;
}
/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 
  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  printf("%d\n", num_pixels_written);
  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}
