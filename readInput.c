#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "ppm_io.h"
#include "imageManip.h"

/* helper function that checks that operation name is valid
 * returns -1 if invalid
 */
int checkname(char *func) {
    if (strcmp(func, "exposure") == 0)
        return 1;
    else if (strcmp(func, "blend") == 0)
        return 2;
    else if (strcmp(func, "zoom_in") == 0)
        return 3;
    else if (strcmp(func, "zoom_out") == 0)
        return 4;
    else if (strcmp(func, "pointilism") == 0)
        return 5;
    else if (strcmp(func, "swirl") == 0)
        return 6;
    else if (strcmp(func, "blur") == 0)
        return 7;
    else
        return -1;
}

/*
 *  Checks if args are appropriate for method call
 *  Returns appropriate error number or 0
 */
int checkargs(int f, int argc, char *argv[]) {
    if (f == 1) { // exposure method
        if (argc == 5) {
            int EV = atof(argv[4]);
            if (EV == 0.0) {
                printf("Incorrect kind of arguments specified for the specified operation\n");
                return 5;
            }
            if ((EV > 3.0) || (EV < -3.0)) {
                printf("Arguments for the specified operation were out of range\n");
                return 6;
            }
        }
	return 0;
    } else if (f == 2) { // blend method
        if (argc != 6) {
            printf("Incorrect number of arguments specified for the specified operation\n");
            return 5;
        } else {
            if (atof(argv[5]) == 0.0) {
                printf("Invalid alpha value");
                return 6;
            }
            FILE *fp = fopen(argv[4], "r");
            if (fp == NULL) {
                fclose(fp);
                printf("No second file found to blend\n");
                return 5;
            }
            fclose(fp);
        }
	return 0;
    } else if (f == 3 || f == 4 || f == 5) { // zoom_in, zoom_out, pointilism methods
        if (argc != 4) {
            printf("Incorrect number of arguments specified for the specified operation\n");
            return 5;
        }
	return 0;
    } else if (f == 6) { // swirl method
        if (argc != 7) {
            printf("Incorrect number of arguments specified for the specified operation\n");
            return 5;
        } else {
            for (unsigned int i = 4; i < 7; i++) {
                double cxD = atof(argv[i]);
                int cx = atoi(argv[i]);
                if(cxD != cx) {
                    printf("Incorrect kind of arguments specified for the specified operation\n");
                    return 5;
                }
            }
        }
	return 0;
    } else if(f == 7) { // blur method
        if (argc != 5) {
            printf("Incorrect number of arguments specified for the specified operation\n");
            return 5;
        } else {
            if ((atof(argv[5]) <= 0.0) || (atof(argv[5]) > 1.0)) {
                printf("Arguments for the specified operation were out of range\n");
                return 6;
            }
        }
	return 0;
    } else {
        return -1;
    }
}

Image * run(Image* img, int func, char *argv[]) {
    if (func == 1) {
      return exposure(img, atof(argv[4]));
    } else if (func == 2) {
        FILE *fp = fopen(argv[4], "r");
        Image *input2 = read_ppm(fp);
        fclose(fp);
	    Image * output = blend(img, input2, atof(argv[5]));
	    free(input2);
        return output;
    } else if (func == 3) {
        return zoom_in(img);
    } else if (func == 4) {
        return zoom_out(img);
    } else if (func == 5) {
        return pointilism(img);
    } else if (func == 6){
        return swirl(img, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]));
    } else if (func == 7) {
        return blur(img, atof(argv[3]));
    } else {
        return NULL;
    }
}

// A helper method that reads in command line from project's main method
// and executes needed imageManip method
int readInput(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Failed to supply input filename or output filename, or both\n");
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("Specified input file could not be opened\n");
        fclose(in);
        return 2;
    }
    Image *img = read_ppm(in);
    fclose(in);

    if (img == NULL) {
        printf("Specified input file is not a properly-formatted PPM file\n");
        free(img);
        return 3;
    }

    if (argc < 4) {
        printf("No operation name was specified\n");
        free(img->data);
        free(img);
        return 4;
    }

    int method = checkname(argv[3]);
    if (method == -1) {
        printf("Operation name specified was invalid\n");
	    free(img->data);
        free(img);
        return 4;
    }

    int check = checkargs(method, argc, argv);
    if (check != 0) {
        free(img->data);
        free(img);
        return check;
    }

    Image* output = run(img, method, argv);
    if (output == NULL) {
        printf("Output not valid PPM\n");
        free(img->data);
        free(img);
        free(output->data);
        free(output);
        return -1;
    } else {
        printf("Output file made successfully\n");
    }

    FILE *out = fopen(argv[2], "wb");
    write_ppm(out, output);

    fclose(out);
    free(output->data);
    free(output);
    free(img);
    return 0;
}
