//
// Created by pteng on 3/28/2020.
//
#include "ppm_io.h"

#ifndef UNTITLED_IMAGEMANIP_H
#define UNTITLED_IMAGEMANIP_H

Image * exposure(Image * input, double  EV);

Image * blend(Image * input1, Image * input2, double alpha);

Image * zoom_in(Image * input);

Image * zoom_out(Image * input);

Image * pointilism(Image * input);

Image * swirl(Image * input, int cx, int cy, int scale);

Image * blur(Image * input, double sigma);

#endif //UNTITLED_IMAGEMANIP_H
